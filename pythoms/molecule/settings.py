# default threshold for low-intensity peak dropping
THRESHOLD = 0.01

# number of peaks to keep for low-intensity peak dropping
NPEAKS = 5000

# consolidation threshold for low-intensity peak combination
CONSOLIDATE = 3

# chatty mode
VERBOSE = False
