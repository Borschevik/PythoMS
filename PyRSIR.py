"""
 Processes a .raw masslynx MS file or previously processed excel file with the parameters provided

 PyRSIR (Python Reconstructed Single Ion Recording; previously SOAPy/PyRSIM)
 pronounced "piercer"

If you use this python script to convert to mzML, you should cite this paper
(of the folks who wrote the msconvert program)

Chambers, M.C. Nature Biotechnology 2012, 30, 918-920
doi: 10.1038/nbt.2377

        TO USE THIS PROGRAM
1) change the working directory to the directory containing the desired *.raw or *.mzML file
2) supply the file name of the *.raw or *.mzML file in quotations in the filename parameter below
3) create an excel file containing a sheet named "parameters" with the desired peaks outlied
   (see below for more details)
   WARNING! This program will keep all data values, but will remove any charts present in the file.
4) supply the file name of the *.xlsx file in quotation in the xlsx parameter below
5) set the number of scans to sum (any positive integer or list of positive integers) in the n parameter below
   (if no summing is desired, set n = 1)

The species names, start m/z, and end m/z values should be contained within a sheet named "parameters"
The first row is ignored for labelling convenience of the user.

Column #1: name of species (this will be the column heading in the output sheets)
    (not needed if the molecular formula is specified)
Column #2: molecular formula of this species
    (not required, but can bypass the need for start and end values)
Column #3: what spectrum to find this species in (+,-,UV)
Column #4: start value (m/z or wavelength)
Column #5: end value (m/z or wavelength)
"""
import argparse
import logging

from pythoms.rsir import RSIR

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(
    description="PyRSIR - Python Reconstructed Single Ion Recorder"
)

parser.add_argument(
    "-fn",
    "--filename",
    action="store",
    type=str,
    help="target mzML file (or file which is convertible to an mzML file)",
    dest="filename",
)

parser.add_argument(
    "-xl",
    "--excel",
    action="store",
    type=str,
    help="target Excel file (with parameters sheet) which defines RSIR targets",
    dest="xlsx",
)

parser.add_argument(
    "-s",
    "--sheet",
    action="store",
    type=str,
    help="sheet name to read rsir targets from",
    dest="sheet",
    default="parameters",
)


parser.add_argument(
    "-n",
    "--num_bins",
    action="store",
    nargs="+",
    type=int,
    help="number of scans to bin. Multiple bin numbers may be specified separated by spaces",
    dest="num_bins",
)

parser.add_argument(
    "--store_ips",
    "--store_isotope_patterns",
    action="store",
    type=bool,
    help="whether to accumulate and isotope patterns (negatively impacts performance)",
    dest="store_ips",
    default=True,
)

args = parser.parse_args()

if __name__ == "__main__":
    # manual override of values
    # input *.raw filename (e.g. 'tests/LY-2015-09-15 06.mzML.gz')
    filename = None

    # Excel file to read from and output to (in *.xlsx format)
    # e.g. 'tests/LY-2015-09-15 06 pyrsir example.xlsx'
    xlsx = None

    # set number of scans to sum (integer or list of integers)
    num_bins = None

    #######
    # instantiate class
    rsir = RSIR(
        filename or args.filename,
        bin_numbers=num_bins or args.num_bins,
        store_isotope_patterns=args.store_ips,
    )

    # add targets as defined in the excel file
    rsir.add_targets_from_xlsx(
        xlsx or args.xlsx, sheet=args.sheet,
    )

    # extract data
    rsir.extract_data()

    # write data to file
    rsir.write_all_data_to_excel(xlsx or args.xlsx)
